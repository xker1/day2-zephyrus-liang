O:

    1. Learn how to do task decomposition, with a simple example to understand how to do task decomposition.Note for task decomposition:

        (1) Splitting complex problem to simple tasks
        (2) Check and make sure tasks are exclusive
        (3) Check and make sure tasks are executive

    2. Learn the composition of Context Map, through a simple exercise, familiar with how to draw a Context Map.The steps to complete a requirement are:

        (1)Requirement
        (2)Analyze
        (3)Tasking
        (4)Context Map
        (5)Coding

    3. Complete the first Coding exercise according to the Context Map.Reviewed Git related knowledge, the use of Git in the development process has four points of understanding:
        (1)Submit frequently in small steps.
        (2)Don’t submit incomplete changes.
        (3)Test changes before submitting.
        (4)Detailed commit message.

R: When learning how to draw Context Map, I felt that task decomposition was difficult.

I: Task decomposition and drawing Context Map are very helpful for development, and the subdivision of various subtasks can help me to do Coding well.

D:I will use task decomposition and Context Map more in future development to help me Coding and improve my development ability.