package pos.machine;

import java.util.ArrayList;
import java.util.List;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItems);
        return renderReceipt(receipt);
    }

    public List<ReceiptItem> decodeToItems(List<String> barcodes) {
        List<Item> items = ItemsLoader.loadAllItems();
        List<ReceiptItem> receiptItems = new ArrayList<>();
        int[] itemCount = new int[items.size()];
        for (String barcode : barcodes) {
            for(int i = 0; i < items.size(); i++) {
                if(items.get(i).getBarcode().equals(barcode)) {
                    itemCount[i]++;
                }
            }
        }
        for(int i = 0; i < items.size(); i++) {
            if(itemCount[i] != 0) {
                ReceiptItem receiptItem = new ReceiptItem(items.get(i).getName(), itemCount[i], items.get(i).getPrice());
                receiptItems.add(receiptItem);
            }
        }
        return receiptItems;
    }

    public List<ReceiptItem> calculateItemsCost(List<ReceiptItem> receiptItems) {
        for(ReceiptItem receiptItem : receiptItems) {
            receiptItem.setSubTotal(receiptItem.getQuantity() * receiptItem.getUnitPrice());
        }
        return receiptItems;
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItems) {
        int totalPrice = 0;
        for(ReceiptItem receiptItem : receiptItems) {
            totalPrice += receiptItem.getSubTotal();
        }
        return totalPrice;
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems) {
        List<ReceiptItem> items = calculateItemsCost(receiptItems);
        return new Receipt(receiptItems, calculateTotalPrice(items));
    }

    public String generateItemsReceipt(Receipt receipt){
        StringBuilder itemsReceipt = new StringBuilder();
        List<ReceiptItem> receiptItems = receipt.getReceiptItems();
        for(ReceiptItem item : receiptItems){
            itemsReceipt.append(item.toString());
            if(item != receiptItems.get(receiptItems.size() - 1)) {
                itemsReceipt.append("\n");
            }
        }
        return itemsReceipt.toString();
    }

    public String generateReceipt(String itemsReceipt, int totalPrice){
        return "***<store earning no money>Receipt***\n" +
                itemsReceipt + "\n" +
                "----------------------\n"
                + "Total: " + totalPrice + " (yuan)\n" +
                "**********************";
    }

    public String renderReceipt(Receipt receipt) {
        String itemsReceipt = generateItemsReceipt(receipt);
        int totalPrice = receipt.getTotalPrice();
        return generateReceipt(itemsReceipt, totalPrice);
    }
}
